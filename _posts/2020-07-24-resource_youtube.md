---
layout: project
title: Videos
icon: fa fa-youtube-play
category: resources
---

- [Computerphile](https://www.youtube.com/user/Computerphile/)
- [CrashCourse: Computer Science](https://www.youtube.com/playlist?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo)
    - Part 1 "The Mechanics of How Computers Work" is a great introduction for ICS51
    - Part 3 "Computer Hardware" is a great introduction for ICS53 & CS143