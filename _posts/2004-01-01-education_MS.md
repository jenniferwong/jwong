---
layout: page
title: MS Thesis
category: education
---

<h4><strong>M.S. Thesis in Computer Science, December 2002</strong></h4>
<b> University of California, Los Angeles, CA <b>

<b>Title:</b> "Non-Parametric Statistical Techniques for Forensic  Engineering"

<b>Thesis Advisor:</b> Professor Miodrag Potkonjak
