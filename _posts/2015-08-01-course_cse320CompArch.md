---
layout: course
course_num: CSE320
title: Computer Architecture
icon: "static/img/microchip_blk.png"
category: sbu
tags:
- 2012 Spring
- 2011 Fall
- 2008 Spring
- 2014 Fall
- 2014 Spring
- 2015 Spring
- 2015 Fall
---

*Course was replaced by CSE320 System Fundamentals II in Fall 2015*

*Last course offering in Fall 2015 as CSE391*

Covers the detailed physical implementation techniques for floating-point data path, advanced pipeline control, multi-level memory hierarchy, I/O and disk subsystem, architectural support for operating systems and programming languages, and multiprocessor/multicomputer architectures.
