---
layout: course
course_num: CSE220
title: Systems Level Programming
icon: "static/img/C_blk.png"
category: sbu
tags:
- 2014 Fall
- 2014 Spring
- 2013 Fall
- 2012 Fall
- 2010 Fall
---

*Course was replaced by CSE220 System Fundementals I in Spring 2015*

Introduces systems-level programming concepts using the C language and assembly language and explores the relation of respective programs in these languages.

Topics covered include:
* internal data representation
* basic instructions and control structures
* arithmetic operations
* pointers
* function calls and parameter passing
* memory allocation
* logical and shift operations
* linking and loading
