---
layout: project
title: Cache Memory Simulator
icon: fa-solid fa-memory
category: project

tags: 
- active
---

The operation of cache memory is a multi-step processes which is often challenging for students to grasp. This project presents a new educational tool built with modern web tooling (React, TypeScript, and TailwindCSS) with the goal of demonstrating the operation of cache accees in a step-by-step manner.

[Website](https://collincaldwelluci.github.io/cache-simulator/)