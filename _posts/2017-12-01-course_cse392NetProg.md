---
layout: course
course_num: CSE392
title: Undergraduate Special Topics - Network Programming
icon: "static/img/eth.png"
category: sbu
courseurl: http://www3.cs.stonybrook.edu/~cse392/
depturl: https://www.cs.stonybrook.edu/students/Undergraduate-Studies/courses/CSE390-394
tags:
- 2018 Spring
---

This course is focused on studying networking protocols and applications, including:
* C / Unix TCP &  UDP socket programming
* Real-time Protocols
* Peer-to-Peer networks
* and lower-level networking concepts.

Assignments and programming projects will focus the course on network programming in the context of network protocol development and implementation (e.g., TCP, ICMP, routing, multicasting, ARP, etc.), and distributed services and ‘system-level’ applications (e.g., client-server and peer-to-peer applications, distributed file systems, name services, etc.).

The goal is to understand the concepts surrounding ‘system-level’ applications and build such applications (e.g., client-server, peer-to-peer, distributed file systems, etc).
