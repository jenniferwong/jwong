---
layout: page
title: PhD Thesis
category: education
---

<h4><strong>Ph.D. Thesis in Computer Science, July 2006</strong></h4>
<b> University of California, Los Angeles, CA <b>

<b>Title:</b> "Design of Embedded Systems using Data-Driven    Statistical Techniques"

<b>Thesis Advisor:</b> Professor Miodrag Potkonjak
