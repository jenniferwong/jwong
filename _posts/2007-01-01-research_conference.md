---
layout: page
title: Conference Papers
category: research
---

[C55] JL. Weber, B. Martinez Neda, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, <strong>J. Wong-Ma</strong>, "WIP: Maximizing Individual Learning Goals Through Customized Student-Project Matching (SPM) in CS Capstone Projects," to appear in Frontiers in Education, FIE, 2024.

[C54] B. Martinez Neda, F. Morales, K. Carbajal Juarez, <strong>J. Wong-Ma</strong>, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, "Investigating the Role of Socioeconomic Factors on CS1 Performance," in IEEE Global Engineering Education, EDUCON, 2024.

[C53]  JL. Weber, B. Martinez Neda, K Carbajal Juarez, <strong>J. Wong-Ma</strong>, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, H Ziv, "Beyond the Hype: Perceptions and Realities of Using Large Language Models in Computer Science Education at an R1 University," in IEEE Global Engineering Education, EDUCON, 2024.

[C52] B. Martinez Neda, J. L. Weber, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, <strong>J. Wong-Ma</strong>, "Enhancing Learning in CS Capstone courses through Advanced Project Matching," in the 17th Annual Conference of the Consortium for Computing Sciences in Colleges: Southwest Region, 2024. 

[C51] B. Martinez Neda, F. Morales, K. Carbajal Juarez, <strong>J. Wong-Ma</strong>, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, "Impacts of Academic Preparedness on CS1 Performance," short paper in the 55th ACM Technical Symposium on Computer Science Education, SIGCSE, 2023.

[C50] JL. Weber, B. Martinez Neda, K. Carbajal Juarez, <strong>J. Wong-Ma</strong>, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, H. Ziv, "Measuring CS Student Attitudes Toward Large Language Models," short paper in the 55th ACM Technical Symposium on Computer Science Education, SIGCSE, 2023.

[C49] B. Martinez Neda, M. Wang, A. Singh, <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, and <strong>J. Wong-Ma</strong>, "Staying Ahead of the Curve: Early Prediction of Academic Probation among First-Year CS Students," in The International Conference on Applied Artificial Intelligence, ICAPAI, 2023.

[C48] B. Martinez Neda, M. Wang, H. Barkam, <strong>J. Wong-Ma</strong>, and <a href="https://faculty.sites.uci.edu/sgagomas/">S. Gago-Masague</a>, "Revisiting Academic Probation in CS: At-Risk Indicators and Impact on Student Success," in 16th Consortium for Computing Sciences in Colleges Southwest Region Conference, CCSC Southwest, 2023.

[C47] M. Luu, M. Ferland, V. N. Rao, A. Arora, R. Huynh, F. Reiber, <strong>J. Wong-Ma</strong>, and <a href="http://www.ics.uci.edu/~mikes/">M. Shindler</a>, "What is an Algorithms Course?: Survey Results of Introductory Undergraduate Algorithms Courses in the U.S.," in 54th ACM Technical Symposium on Computer Science Education, SIGCSE, 2023.

[C46] J. Mitchener, N. Beeken, and <strong>J. L. Wong</strong>, "Dashmips: MIPS interpreter and VSCode Debugger," in 52nd ACM Technical Symposium on Computer Science Education, SIGCSE, 2021.

[C45] M. Ehsan, Y. Chen, H. Kang, R. Sion, and <strong>J. L. Wong</strong>, "Lips: A cost-efficient data and task co-scheduler for mapreduce," in 20th Annual International Conference on High Performance Computing, HiPC, 2013.

[C44] H. Kang and <strong>J. L. Wong</strong>, "vCSIMx86: a Cache Simulation Framework for x86 Virtualization Hosts," to appear in, Third Workshop on Runtime Environments/Systems, Layering, and Virtualized Environments (RESoLVE), 2013.

[C43] H. Kang and <strong>J. L. Wong</strong>, "To hardware prefetch or not to prefeth: A virtualized environment study &amp; core binding approach," to appear in, Eighteenth International Conference on Architectural Support for Programming Languages and Operating Systems (ASPLOS), 2013.

[C42] H. Kang, X. Zhu, and <strong>J. L. Wong</strong>, "Dapa: Diagnosing application performance anomalies for virtualized infrastructures," in 2nd USENIX Workshop on Hot Topics in Management of Internet, Cloud, and Enterprise Networks and Services (Hot-ICE), 2012, pp. 8-8.

[C41] H. Kang, Y. Chen, <strong>J. L. Wong</strong>, R. Sion, and J. Wu, "Enhancement of Xen&#8217;s scheduler
for mapreduce workloads," in International Symposium on High Performance Distributed Computing, June 2011, pp. 251-262.

[C40] A. Bhattacharyya and <strong>J. L. Wong</strong>, "A resilient actuation attack on wireless sensor network," in International Conference on Sensor Networks and Applications, November 2010, pp. 251-256.

[C39] <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, S. Meguerdichian, and <strong>J. L. Wong</strong>, "Trusted sensors and remote sensing," in IEEE Conference on Sensors, November 2010, pp. 1104 - 1107.

[C38] T. Giridher, <a href="http://www3.cs.stonybrook.edu/~anita/"><a href="http://www3.cs.stonybrook.edu/~anita/">A. Wasilewska</a></a>, <strong>J. L. Wong</strong>, and K. S. Rekhi, "Global mobile applications for monitoring health," in International Multiconference on Computer Science and Information Technology, October 2010, pp. 855-859.

[C37] T. Giridher, A. Bulchandani, R. Kim, P. Naik, <a href="http://www3.cs.stonybrook.edu/~anita/"><a href="http://www3.cs.stonybrook.edu/~anita/">A. Wasilewska</a></a>, and <strong>J. L. Wong</strong>, "Social mobile applications," in IEEE The Sixth Annual Conference on Long Island Systems, Applications and Technology, May 2010, pp. 1-6.

[C36] <a href="http://www3.cs.stonybrook.edu/~anita/"><a href="http://www3.cs.stonybrook.edu/~anita/">A. Wasilewska</a></a> and <strong>J. L. Wong</strong>, "Developing template applications for social advancement," in 1st International Symposium on Intelligent Mobile Technologies for Social Change at International Multiconference on Computer Science and Information Technology, October 2009, pp. 391-398.

[C35] <a href="http://csis.pace.edu/~scharff/">C. Scharff</a>, <a href="http://www3.cs.stonybrook.edu/~anita/"><a href="http://www3.cs.stonybrook.edu/~anita/">A. Wasilewska</a></a>, <strong>J. L. Wong</strong>, M. Bousso, and I. Ndiaye, "A model for teaching mobile application development for social changes: Implementation and lessons learned in senegal," in 1st International Symposium on Intelligent Mobile Technologies for Social change at International Multiconference on Computer Science and Information Technology, October 2009, pp. 383-389.

[C34] T. Giridher, R. Kim, D. Rai, A. Hanover, J. Yu, F. Zarinni, <a href="http://csis.pace.edu/~scharff/">C. Scharff</a>, <a href="http://www3.cs.stonybrook.edu/~anita/">A. Wasilewska</a>, and <strong>J. L. Wong</strong>, "Mobile applications for informal economies," in 1st International Symposium on Intelligent Mobile Technologies for Social Change at International Multiconference on Computer Science and Information Technology, October 2009, pp. 345-352.

[C33] S. P. Tinta, Y. Zhou, and <strong>J. L. Wong</strong>, "Robot-assisted energy-efficient data collection from high-fidelity sensor networks," in International Conference on Technologies for Practical Robot Applications, November 2009, pp. 101-106.

[C32] S. P. Tinta, A. E. Mohr, and <strong>J. L. Wong</strong>, "Characterizing end-to-end packet reordering with UDP traffic," in IEEE Symposium on Computers and Communications, July 2009, pp. 321-324.

[C31] H. Kang and <strong>J. L. Wong</strong>, "A localized multi-hop desynchronization algorithm for wireless sensor networks," in IEEE Infocom, April 2009, pp. 2906-2910.

[C30] A. Singh,&nbsp;<a href="http://www.cs.sunysb.edu/%7Ecram/">CR Ramnakrishnan</a>, <a href="http://www.cs.sunysb.edu/%7Eram/">IV   Ramnakrishnan</a>,<a href="http://www.cs.sunysb.edu/%7Ewarren/"> D. Warren</a>, and <span style="font-weight: bold;">J. L. Wong</span>, "A methodology for in-network evaluation of integrated logical-statistical models," to appear in IEEE Sensys, 2008.

[C29] <span style="font-weight: bold;">J. L.Wong</span>, <a href="http://www.engr.wisc.edu/ece/faculty/davoodi_azadeh.html">A. Davoodi</a>, V. Khandelwal, <a href="http:// ww.ece.umd.edu/%7Eankurs/">A. Srivastava</a>, and&nbsp;<a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, "Statistical timing analysis using kernel smoothing," in International Conference on Computer Design, 2007, pp. 97-102.

[C28]&nbsp;<a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a> and <span style="font-weight: bold;">J. L. Wong</span>, Introduction to digital design: A paradigm-based approach," in IEEE International Conference on Microelectronic Systems Education, 2007, pp. 167-168.

[C27] <span style="font-weight: bold;">J. L. Wong</span>, <a href="http://www.seapahn.com/">S. Megerian</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, "Symmetric monotonic regression: Techniques and applications for sensor networks," in IEEE Sensors Applications Symposium, 2007, pp. 1-6.

[C26]&nbsp;<b>J. L. Wong</b>, <a href="http://www.seapahn.com/">S. Megerian</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, , "Minimizing global interconnect in DSP systems using bypassing," in IEEE International Conference on Acoustics, Speech, and Signal Processing, vol. 2, 2007, pp. 77-80.

[C25] <b>J. L. Wong</b>, <a href="http://www.seapahn.com/">S. Megerian</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, "Staggered Sampling for Efficient Data Collection," in IEEE Conference on Sensors, 2006, pp. 777-780.

[C24] <b>J. L. Wong</b>, <a href="http://eceweb.ucsd.edu/~fkoushanfar/">F. Koushanfar</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, "Flexible ASIC: shared masking for multiple media processors," in DAC '05: Proceedings of the 42th ACM/IEEE Conference on Design Automation, 2005, pp. 909-914.


[C23] A. Cerpa, <b>J. L. Wong</b>, <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> and <a href="http://destrin.smalldata.io/">D. Estrin</a>, "Temporal properties of low-power wireless links: Modeling and implications on multi-hop routing," in ACM International Symposium on Mobile Ad Hoc Networking and Computing, 2005, pp. 414-425.


[C22] A. Cerpa, <b>J. L. Wong</b>, L. Kuang, <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> and <a href="http://destrin.smalldata.io/">D. Estrin</a>, "Statistical model of lossy links in wireless sensor networks," in IEEE/ACM International Conference on Information Processing in Sensor Networks, 2005, pp. 81-88.


[C21] <b>J. L. Wong</b>, W. Liao, F. Li, L. He, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Scheduling of soft real-time systems for context-aware applications," in International Conference on Design, Automation and Test in Europe, 2005, pp. 318-323.


[C20] <b>J. L. Wong</b>, R. Jafari, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Gateway placement for latency and energy efficient data aggregation," in LCN '04: Proceedings of the 29th Annual IEEE
International Conference on Local Computer Networks (LCN'04). IEEE
Computer Society, 2004, pp. 490-497.



[C19] <b>J. L. Wong</b>, J.-Q. Yao, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Watermarking multiple constant multiplications solutions," in Asilomar Conference on Signals, Systems, and Computers, 2004, pp. 67-71.


[C18] <b>J. L. Wong</b> and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Relative generic computational forensic techniques," in IHW '04: Proceedings of the 6th International Information Hiding Workshop,
2004, pp. 148-163.

[C17] <b>J. L. Wong</b>, <a href="http://www.engr.wisc.edu/ece/faculty/davoodi_azadeh.html">A. Davoodi</a>, V. Khandelwal, <a href="http://www.ece.umd.edu/%7Eankurs/">A. Srivastava</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Wire-length prediction using statistical techniques," in IEEE/ACM International Conference on Computer Aided Design. IEEE Press, 2004, pp. 702-705.



[C16] <b>J. L. Wong</b>, <a href="http://www.seapahn.com/">
  <a href="http://www.seapahn.com/">S. Megerian</a></a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Design techniques for sensor appliances: foundations and
light compass case study," in DAC '03: Proceedings of the 40th Conference
on Design Automation. ACM Press, 2003, pp. 66-71.



[C15] <b>J. L. Wong</b>, <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "An on-line approach for power minimization in QoS sensitive systems," in ASP-DAC '03: Proceedings of the 2003 Conference on Asia South Pacific Design Automation. ACM Press, 2003, pp. 59-64.



[C14] <b>J. L. Wong</b> and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Search in sensor networks: challenges, techniques, and applications," in IEEE International Conference on Acoustics, Speech, and Signal Processing, vol. IV. IEEE Press, 2002, pp. 3752-3755.


[C13] <b>J. L. Wong</b>, G. Veltri, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.   Potkonjak,</a> "Energy-efficient event tracking in multi-hop wireless networks," in Integrated Management of Power Aware Communications, Computing and Networking (IMPACCT), 2002, pp. 69-85.

[C12] <b>J. L. Wong</b>, <a href="http://www.ece.umd.edu/%7Egangqu/"> G.
  Qu</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a>
"Power minimization under QoS constraints," in IEEE International
Packet-video Workshop, 2002, pp. 22-1 - 22-10.

[C11]
<a href="http://eceweb.ucsd.edu/~fkoushanfar/">F. Koushanfar</a>, <b>J. L. Wong</b>,
  J. Feng, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "ILP-based engineering change," in DAC '02: Proceedings
of the 39th Conference on Design Automation. ACM Press, 2002, pp. 910-915.


[C10] <b>J. L. Wong</b>, <a href="http://www.seapahn.com/">
  <a href="http://www.seapahn.com/">S. Megerian</a></a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Forward-looking objective functions: concept and
applications in high level synthesis," in DAC '02: Proceedings of the 39th
Conference on Design Automation. ACM Press, 2002, pp. 904-909.


[C9] <b>J. L. Wong</b>, <a href="https://www.linkedin.com/in/kirovski">D.
  Kirovski</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Computational forensic techniques for intellectual
property protection," in IHW '01: Proceedings of the 4th International
Workshop on Information Hiding. Springer-Verlag, 2001, pp. 66-80.

[C8] <b>J. L. Wong</b>,
<a href="http://eceweb.ucsd.edu/~fkoushanfar/">F. Koushanfar</a>, <a href="http://www.seapahn.com/">
  S. Meguerdichian</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "A probabilistic constructive approach to optimization
problems," in ICCAD '01: Proceedings of the 2001 IEEE/ACM International
Conference on Computer-Aided Design. IEEE Press, 2001, pp. 453-456.

[C7] G. Wolfe, <b>J. L. Wong</b>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Watermarking graph partitioning solutions," in DAC '01:
Proceedings of the 38th Conference on Design Automation. ACM Press, 2001,
pp. 486-489.

[C6] R. Majumdar and <b>J. L. Wong</b>, "Watermarking of SAT using
combinatorial isolation lemmas," in DAC '01: Proceedings of the 38th
Conference on Design Automation. ACM Press, 2001, pp. 480-485.


[C5] <a href="https://www.linkedin.com/in/kirovski">D. Kirovski</a>, D. Liu,
<b>J. L. Wong</b>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Forensic engineering techniques for VLSI CAD tools," in
DAC '00: Proceedings of the 37th Conference on Design Automation. ACM
Press, 2000, pp. 580-586.


[C4] <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, <b>J. L.
  Wong</b>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
  Potkonjak,</a> "Fair watermarking techniques," in ASP-DAC '00:
Proceedings of the 2000 Conference on Asia South Pacific Design
Automation. ACM Press, 2000, pp. 55-60.


[C3] A. B. Kahng, <a href="https://www.linkedin.com/in/kirovski">D. Kirovski</a>,
S. Mantik, <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a>
and <b>J. L. Wong</b>, "Copy detection for intellectual property
protection of VLSI designs," in ICCAD '99: Proceedings of the 1999
IEEE/ACM International Conference on Computer-aided design. IEEE Press,
1999, pp. 600-605.

[C2] A. E. Caldwell, H.-J. Choi, A. B. Kahng, S. Mantik, <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, and <b>J. L. Wong</b>, "Effective iterative techniques for fingerprinting design IP," in DAC '99: Proceedings of the 36th ACM/IEEE Conference on Design Automation. ACM Press, 1999, pp. 843-848.


[C1] <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, <b>J. L. Wong</b>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Optimization-intensive watermarking techniques for decision problems," in DAC '99: Proceedings of the 36th ACM/IEEE Conference on Design Automation. ACM Press, 1999, pp. 33-36.