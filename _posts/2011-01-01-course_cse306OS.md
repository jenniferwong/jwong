---
layout: course
course_num: CSE306
title: Operating Systems
icon: "static/img/os_blk.png"
category: sbu
depturl: https://www.cs.stonybrook.edu/students/Undergraduate-Studies/courses/CSE306
tags:
- 2011 Spring
---

Students are introduced to the structure of modern operating systems. Topics include virtual memory, resource allocation strategies, concurrency, and protection. The design and implementation of a simple operating system are performed.