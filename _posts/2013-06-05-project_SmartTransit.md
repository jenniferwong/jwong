---
layout: project
title:  SBUSmartTransit
icon: fa fa-bus
category: project

tags: 
- old
---

SBU Smart Transit was the Stony Brook University student-developed passenger information system for SBU Transit. SBU Smart Transit was developed through a partnership between the Stony Brook University Department of Transportation and Parking Operations, the Center of Excellence in Wireless and Information Technology (CEWIT) and the College of Engineering and Applied Sciences and was a comprehensive bus and shuttle global positioning system that provides live vehicle location, bus stop information and estimated arrival times to passengers using web and mobile-based applications.

Hardware and software for the project was 100% built and maintained by Stony Brook students through coursework and summer internships. Starting in 2011, the project provided approximately 50 students the opportunity to work on a full-stack development project. Configured embedded devices and third-party hardware/software integration was used to provide real-time tracking of approximately 50 campus vehicles. A developed web management system allows Transportation and Facilities Services on campus to managed the visibility of the fleet in real-time.

