---
layout: page
title: Journal Papers
category: research
---

[J10] E.S. Park, A. Harlow, A. AghaKouchak, B. Baldi, N. Burley, N. Buswell, R. Crooks, D. Denenberg, P. Ditto, K. Edwards, M. G. Junqueira, A. Geragotelis, A. Holton, J. Lanning, R. Lehman, A. Chen, A. Pantano, J. Rinehart, M. Walter, A. Williams, <b>J. Wong-Ma</b>, M. Yassa, and B. Sato, "Instructor facilitation mediates students’ negative perceptions of active learning instruction," PLoS ONE, vol. 16, no. 12, December 2021.

[J9] <b>J. L. Wong</b>, <a href="http://www.engr.wisc.edu/ece/faculty/davoodi_azadeh.html">A. Davoodi</a>, V. Khandelwal, and <a href="http://www.ece.umd.edu/%7Eankurs/">A. Srivastava </a>and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a> , "A statistical methodology for wire-length prediction," IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, 2006.

[J8] <b>J. L. Wong</b>, <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a> , "Power minimization in QoS sensitive systems," IEEE Transactions on Very Large Scale Integration (VLSI) Systems, vol. 12, no. 6, pp. 553-561, 2004.

[J7] <b>J. L. Wong</b>, R. Majumdar, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a> , "Fair watermarking using combinatorial isolation lemmas," IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol. 23, no. 11, pp. 1566-1574, 2004.


[J6] <b>J. L. Wong</b>, <a href="http://eceweb.ucsd.edu/~fkoushanfar/">F. Koushanfar</a>, <a href="http://www.ece.wisc.edu/%7Emegerian/"> S. Megerian</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.  Potkonjak</a> , "Probabilistic constructive optimization techniques," IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol. 23, no. 6, pp. 859-868, 2004.


[J5] <b>J. L. Wong</b>, <a href="http://www.cs.ucla.edu/%7Edarko/">D.
Kirovski</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
Potkonjak</a> , "Computational forensic techniques for intellectual
property protection," IEEE Transactions on Computer-Aided Design of
Integrated Circuits and Systems, vol. 23, no. 6, pp. 987-994, 2004.

[J4] A. Caldwell, H.-J. Choi, A. Kahng, S. Mantik, <a href="http://www.cs.ucla.edu/%7Emiodrag/">M.
Potkonjak</a> , <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, and <b>J. L. Wong</b>, "Effective iterative techniques for fingerprinting design IP," IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol. 23, no. 2, pp. 208-215, 2004.


[J3] <b>J. L. Wong</b>, <a href="http://www.ece.umd.edu/%7Egangqu/"> G. Qu</a>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a>, "Optimization-intensive watermarking techniques for decision problems,"  IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol. 23, no. 1, pp. 119-127, 2004.

[J2] <b>J. L. Wong</b>, <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a> , and S. Dey, "Optimizing designs using the addition of deflection operations," IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol. 23, no. 1, pp. 50-59, 2004.

[J1] G. Wolfe, <b>J. L. Wong</b>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak</a> , "Watermarking graph partitioning solutions," IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol. 21, no. 10, pp. 1196-1204, 2002.