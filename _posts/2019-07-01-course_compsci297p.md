---
layout: course
course_num: COMPSCI 297P
title: Capstone Design Project for Computer Science
icon: "static/img/skill.png"
category: teaching

tags:
- 2019 Fall
---

The course is taught in MCS students’ last Fall quarter, concurrently with CS 296P Writing and Communication.

- The design project will involve taking a new idea from conception to prototype development and validation.
- Projects will draw on skills learned in several of the courses in the curriculum and will be initiated by computer science faculty or by our corporate affiliates. Students will form teams of two-four students and their work will be supervised by both the instructor of the course and a faculty project mentor who will be involved in all aspects of the project.
- The scope of the projects will include physical prototype development, appropriate testing and detailed documentation. Ideally projects will represent a complete system or product, integrating analysis, simulation, and software and hardware design as appropriate.
