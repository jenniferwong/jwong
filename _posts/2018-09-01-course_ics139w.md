---
layout: course
course_num: ICS 139W
title: Critical Writing on Information Technology
icon: "static/img/compose_blk.png"
category: teaching

tags:
- 2018 Fall
- 2019 Winter
---

Study and practice of critical writing and oral communication as it applies to information technology. Each student writes assignments of varying lengths, totaling at least 4,000 words.
