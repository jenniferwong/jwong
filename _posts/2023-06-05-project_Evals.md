---
layout: project
title: Student Evaluations - Summary and Sentiment
icon: fa-solid fa-magnifying-glass-chart
category: project

tags: 
- active
---

Student feedback plays a crucial role in course pedagogy and creating inclusive learning environments. Student evaluations of teaching and mid-term feedback surveys continue to be the de facto standard for collecting numerical and written comments. Courses with large enrollments, particularly introductory courses, are often left in the dark about true student perceptions and feedback due to the sheer volume of responses. Additionally, instructors may gain a false sense of the population based on the "loud" feedback within the written comments or the opinions of the small subset of students. 

The goal of this project is to develop tools to assist faculty in deciphering the overall themes and sentiment from the student feedback in order to improve their teaching. 