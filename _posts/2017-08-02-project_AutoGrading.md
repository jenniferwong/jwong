---
layout: project
title: Scalable Grading and Submission Systems
icon: fa fa-cogs
category: project

tags: 
- active
---

One of the challenges of teaching large systems courses is managing submission and grading of course programming assignments.
Since 2015, I have been working with a set of undergraduate and graduate student to develop autograding tools to reduce grading overhead, enable equitable grading practices, and provide continual student feedback during development. The foundation of the project utilizes unit-testing to enable both accurate, as well as incremental/partial grading. The goal is to instill good practices and to encourage students to think about edge cases when testing, as well as provide positive feedback as they progress through the assignment. 

Past iterations this project include "git-submit," which creates and manages a private submit branch within the students' git repositories. A python-based grading framework facilitated container-based testing of each unit test on a student assignment. The framework provided an interface between different types of unit testing environments and gathered all grading information to build a single uniform gradesheet with feedback. We utilized git continuous integration to ensure student submissions compile and execute minimal testcases alerting students immediately to any concerns with their submission. 

The current version is a container-based multi-threaded autograder with a variety of unit test options, including validation of multiple program output streams and system state. In the case of test failure students are presented with both the expected and obtained output in one of many available formats (eg. byte-by-byte, character strings, array values, etc). With each term, improvements and features are added to the systems to increase functionality, flexibility, and accuracy. We incorporate student feedback into the tools to improve usability and user experience.