---
layout: project
title: Websites
icon: fa fa-rss
category: resources
---

- [Julia Evans](https://jvns.ca/)
    - She creates [Wizard Zines](https://wizardzines.com/) which are practical comics on programming
    - Blog posts with insight and tutorials on computer/programming/systems
    - [Twitter Comics](https://twitter.com/b0rk)
