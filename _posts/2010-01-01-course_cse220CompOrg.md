---
layout: course
course_num: CSE220
title: Computer Organization
icon: "static/img/microchip_blk.png"
category: sbu
tags:
- 2007 Fall
- 2010 Spring
---

*Course was replaced by CSE220 System-level Programming in Fall 2010*

Explores the physical structure of a computer; internal representation of information; processor organization, instruction cycle, and memory hierarchy. Introduces assembly/machine language programming and its relation to execution of high level language programs. Elementary digital logic design and its application to design of arithmetic and logic unit, and simple data paths. Input and output devices and their interface with processor and memory

Topic Covered:
*   Concept of a stored program computer.
*   Bits, Bytes and Words.
*   Binary numbers and their arithmetic.
*   Floating point numbers and their arithmetic.
*   Review of C programming language.
*   Memory read/write operations.
*   Fetch-Decode-Execute cycle.
*   Evolution of computer architecture.
*   MIPS assembly language.
*   Addressing modes.
*   Procedures and Parameter passing.
*   Two-pass assembly process.
*   Linking and Loading.
*   Elementary logic design: Flip-Flops, Concept of a register, CPU clock. 1-bit and n-bit ALU.
*   MIPS data path.
*   Multiplication of two binary numbers.
*   Computer buses.
*   Simple I/O devices and I/O programming.
*   Introduction to Cache and Virtual memory

