---
layout: page
title: Book Chapters
category: research
---
[B2] S. Slijepcevic, <b>J. L. Wong</b>, and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a> "Security and privacy protection in wireless sensor
networks," Handbook of Sensor Networks: Compact Wireless and Wired Sensing
Systems, pp. 31-1 - 31-15, 2004. <br>
<br>
[B1] <b>J. L. Wong</b>, J. Feng, <a href="https://www.linkedin.com/in/kirovski">D. Kirovski</a>,
and <a href="http://www.cs.ucla.edu/%7Emiodrag/">M. Potkonjak,</a>
"Security in sensor networks: watermarking techniques," Wireless Sensor
Networks, pp. 305-323, 2004. <br>