---
layout: page
title: Presentations
category: presentation
---

[P15] "Flexible ASIC: Shared Masking for Multiple Media Processors."
Presented at the 42nd ACM/IEEE Design Automation Conference, Anaheim, CA, June 2005.

[P14] "Wire Length Prediction Using Statistical Techniques." Presented at
IEEE/ACM International Conference on Computer-Aided Design, San Jose, CA, November 2004.

[P13] "Watermarking Multiple Constant Multiplication Solutions." Presented
at Asilomar Confernce on Signals, Systems, and Computers,Pacific Grove, CA, November 2004.

[P12] "Relative Generic Computational Forensic Techniques." Presented at
Information Hiding, Toronto, Canada, May 2004.

[P11] "Minimizing Global Interconnect using Bypassing and Chaining."
Presented at UCLA Research Review, Los Angeles, CA, April 2004.

[P10] "Design Techniques for Sensor Appliances: Foundations and Light
Compass Case Study." Presented at 39th ACM/IEEE Design Automation      Conference, Anaheim, CA, June 2003.


[P9] "Design and Analysis of Mobile Systems." Presented at UCLA Research      Review, Los Angeles, CA, April 2003.


[P8] "On-line Approach to Power Minimization in QoS Sensitive Systems."      Presented at IEEE/ACM Asia and South Pacific Design Automation Conference,
Kitakyushu, Japan, January 2003.

[P7] "Forward Looking Objective Functions: Concept &amp; Applications In
High Level Synthesis." Presented at 39th ACM/IEEE Design Automation      Conference, New Orleans, LA, June 2002.

[P6] "Search in Sensor Networks." Presented at ICASSP, Orlando, FL, May      2002.

[P5] "A Probabilistic Constructive Approach to Optimization Problems."
Presented at IEEE/ACM International Conference on Computer-Aided Design,      San Jose, CA, November 2001.

[P4] "Watermarking of SAT using Combinatorial Isolation Lemmas." Presented at 38th ACM/IEEE Design Automation Conference, Las Vegas, NV, June 2001.

[P3] "Forensic Engineering Techniques for VLSI CAD Tools." Presented at 37th ACM/IEEE Design Automation Conference, Los Angeles, CA, June 2000.

[P2] "Non-Parametrical Statistical Computational Forensic Techniques for Intellectual Property Protection." Presented at 4th International Information Hiding Workshop, Pittsburgh, PA, April 2001.

[P1] "Effective Iterative Techniques for Fingerprinting Design IP."
Presented at 36th ACM/IEEE Design Automation Conference, New Orleans, LA, June 1999.