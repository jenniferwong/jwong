---
layout: project
title: Educational Gitlab Extension for courses
icon: fa fa-git-square
category: project

tags: 
- inactive
---

Git is taught/utilized in our system and development courses for projects assignments. Integration and exposure to industry standard tools is highly beneficial to the students, however these tools do not support the required course management features.

The goal of this new project is to extend the Gitlab open-source community edition to support front-end features for assignment submissions and automated individual/group repository creation from roster.

**Interested in working on this project?** Send me email to discuss further.