---
layout: project
title: Real-time GPS Arrival Predictions using Machine Learning Techniques
icon: fa fa-map-o
category: project

tags: 
- old
---

As part of the continued improvements to Stony Brook’s SmartTransit system, a pair of CSE Honors students are using machine learning techniques to automatically detect in real-time route assignments for Stony Brook’s buses. The goal of the project was to use both historical and real-time data to enable a real-time route detection/assignment tool and integrate this tool into the current production Smart Transit Management system. A real-time notification system would provide transit managers with discrepancy between the route taken via the vehicle and the assigned route. Additionally, this system will be used to further refine our departure/arrival time calculations which are presented directly to the user.

